#1 Grafica de lineas
import pandas as pd
import matplotlib.pyplot as plt

# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Filtrar los vinos de buena calidad
df_good_quality = df[df['quality'] >= 7]

# Filtrar los vinos de mala calidad
df_bad_quality = df[df['quality'] < 4]

# Obtener las variables independientes
variables = ['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
             'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']

# Crear el gráfico de líneas
plt.figure(figsize=(12, 6))

# Graficar las líneas para los vinos de buena calidad
plt.plot(variables, df_good_quality[variables].mean(), marker='o', label='Buena calidad')

# Graficar las líneas para los vinos de mala calidad
plt.plot(variables, df_bad_quality[variables].mean(), marker='o', label='Mala calidad')

# Añadir etiquetas y título
plt.xlabel('Variable')
plt.ylabel('Nivel')
plt.title('Comparación de Niveles de Variables entre Vinos de Buena y Mala Calidad')

# Rotar los nombres de las variables en el eje x
plt.xticks(rotation=20)

# Añadir leyenda
plt.legend()

# Mostrar el gráfico
plt.show()



#2Matriz de correlación

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Seleccionar las variables numéricas para calcular la matriz de correlación
variables = ['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
             'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol', 'quality']

# Calcular la matriz de correlación
corr_matrix = df[variables].corr()

# Crear el mapa de calor de la matriz de correlación
plt.figure(figsize=(10, 8))
sns.heatmap(corr_matrix, annot=True, cmap='coolwarm', fmt=".2f")
plt.title('Matriz de Correlación')
plt.show()

# 3 Distribucion de variables

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Seleccionar las variables numéricas
variables = ['free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']

# Configurar la figura y los subplots
fig, axes = plt.subplots(nrows=len(variables), ncols=2, figsize=(10, 20))
fig.subplots_adjust(hspace=0.5)

# Iterar sobre las variables y graficar histogramas y gráficos de densidad
for i, variable in enumerate(variables):
    # Histograma
    axes[i, 0].hist(df[variable], bins=30, color='skyblue', edgecolor='black')
    axes[i, 0].set_title(f'Histograma de {variable}')
    axes[i, 0].set_xlabel(variable)
    axes[i, 0].set_ylabel('Frecuencia')

    # Gráfico de densidad
    sns.kdeplot(df[variable], ax=axes[i, 1], color='orange', fill=True)
    axes[i, 1].set_title(f'Gráfico de Densidad de {variable}')
    axes[i, 1].set_xlabel(variable)
    axes[i, 1].set_ylabel('Densidad')

# Mostrar los gráficos
plt.show()

#4 Graficos de dispersion

import pandas as pd
import matplotlib.pyplot as plt

# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Seleccionar las variables que deseas graficar
variables = ['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
             'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']

# Generar los gráficos de dispersión
for variable in variables:
    plt.scatter(df[variable], df['quality'], alpha=0.5)
    plt.xlabel(variable)
    plt.ylabel('quality')
    plt.title(f'Gráfico de dispersión: {variable} vs quality')
    plt.show()