import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Seleccionar las columnas que vamos a utilizar en la regresión lineal
X = df[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = df['quality']

# Normalizar las características
scaler = StandardScaler()
X = scaler.fit_transform(X)

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Crear el modelo de regresión lineal y entrenarlo
model = LinearRegression()
model.fit(X_train, y_train)

# Hacer las predicciones
y_pred = model.predict(X_test)

# Calcular el error cuadrático medio y el coeficiente de determinación (R²)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)

# Imprimir los resultados
print("Error cuadrático medio:", mse)
print("Coeficiente de determinación (R²):", r2)

# Crear el gráfico de dispersión de los valores reales y las predicciones
plt.scatter(y_test, y_pred, alpha=0.5)
plt.xlabel('Valores reales')
plt.ylabel('Predicciones')
plt.title('Regresión lineal')
plt.show()