import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.model_selection import cross_val_score, GridSearchCV
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import learning_curve
from sklearn.model_selection import validation_curve
from sklearn.metrics import roc_curve, auc


# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Convertir la variable objetivo "quality" en una variable binaria
df['quality'] = np.where(df['quality'] > 6.5, 1, 0)

# Seleccionar las columnas que vamos a utilizar en el modelo de K-Nearest Neighbors
X = df[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = df['quality']

# Normalizar las características
scaler = StandardScaler()
X = scaler.fit_transform(X)

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Definir los parámetros a buscar con GridSearchCV
params = {
    'n_neighbors': range(1, 50),
    'weights': ['uniform', 'distance'],
    'metric': ['euclidean', 'manhattan', 'minkowski']
}

# Crear el modelo de K-Nearest Neighbors
model = KNeighborsClassifier()

# Buscar los mejores parámetros con GridSearchCV
grid_search = GridSearchCV(model, params, cv=5)
grid_search.fit(X_train, y_train)

# Entrenar el modelo con los mejores parámetros
best_model = grid_search.best_estimator_
best_model.fit(X_train, y_train)

# Hacer las predicciones
y_pred = best_model.predict(X_test)

# Calcular las métricas de rendimiento
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred, average='weighted')
recall = recall_score(y_test, y_pred, average='weighted')
f1 = f1_score(y_test, y_pred, average='weighted')
confusion = confusion_matrix(y_test, y_pred)


# Imprimir los resultados
print("Exactitud:", accuracy)
print("Precisión:", precision)
print("Recall:", recall)
print("F1-score:", f1)
print("Matriz de confusión:\n", confusion)

# Graficar la matriz de confusión
sns.heatmap(confusion, annot=True, fmt='d', cmap='Blues')
plt.xlabel('Predicted')
plt.ylabel('Actual')
plt.title('Matriz de Confusión - KNN')
plt.show()

# curva de aprendizaje
def plot_learning_curve(model, X, y):
    train_sizes, train_scores, test_scores = learning_curve(model, X, y, cv=5, scoring='accuracy', n_jobs=-1)

    # Calcular las medias y desviaciones estándar de las puntuaciones
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    # Graficar las curvas de aprendizaje
    plt.figure()
    plt.plot(train_sizes, train_scores_mean, 'o-', color='r', label='Entrenamiento')
    plt.plot(train_sizes, test_scores_mean, 'o-', color='g', label='Validación cruzada')
    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.2,
                     color='r')
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.2,
                     color='g')
    plt.xlabel('Tamaño del conjunto de entrenamiento')
    plt.ylabel('Puntuación de precisión')
    plt.title('Curvas de Aprendizaje - KNN')
    plt.legend()
    plt.grid(True)
    plt.show()

plot_learning_curve(model, X_train, y_train)

# Curva de validación
def plot_validation_curve(model, X, y, param_name, param_range):
    train_scores, test_scores = validation_curve(model, X, y, param_name=param_name, param_range=param_range,
                                                 cv=5, scoring='accuracy', n_jobs=-1)

    # Calcular las medias y desviaciones estándar de las puntuaciones
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    # Graficar las curvas de validación
    plt.figure()
    plt.plot(param_range, train_scores_mean, 'o-', color='r', label='Entrenamiento')
    plt.plot(param_range, test_scores_mean, 'o-', color='g', label='Validación cruzada')
    plt.fill_between(param_range, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.2,
                     color='r')
    plt.fill_between(param_range, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.2,
                     color='g')
    plt.xlabel(param_name)
    plt.ylabel('Puntuación de precisión')
    plt.title('Curva de Validación - KNN')
    plt.legend()
    plt.grid(True)
    plt.show()

# Graficar la curva de validación para el hiperparámetro 'n_neighbors'
param_range = range(1, 50)
plot_validation_curve(best_model, X_train, y_train, param_name='n_neighbors', param_range=param_range)

# Calcular la curva ROC
y_pred_prob = best_model.predict_proba(X_test)[:, 1]
fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob)
roc_auc = auc(fpr, tpr)

# Graficar la curva ROC
plt.figure()
plt.plot(fpr, tpr, color='darkorange', lw=2, label='Curva ROC (AUC = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('Tasa de falsos positivos')
plt.ylabel('Tasa de verdaderos positivos')
plt.title('Curva ROC - KNN')
plt.legend(loc="lower right")
plt.show()