import pandas as pd
import matplotlib.pyplot as plt

# leer el archivo CSV
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# crear un gráfico de barras para cada variable
for column in df.columns[:-1]:
    # agrupar por calidad (categórica) y obtener la media de la variable (numérica)
    variable_por_calidad = df.groupby('quality')[column].mean()

    # crear el gráfico de barras
    fig, ax = plt.subplots()
    ax.bar(variable_por_calidad.index, variable_por_calidad.values)
    ax.set_xlabel('Calidad')
    ax.set_ylabel(column)
    ax.set_title(f'Relación entre calidad y {column}')
    plt.show()

    # Imprimir el título de la gráfica
    print(f"Título de la gráfica: Relación entre calidad y {column}")

    # Imprimir el resultado por consola
    for quality, value in variable_por_calidad.items():
        print(f"Nivel {quality} de calidad: {value}")
    print()