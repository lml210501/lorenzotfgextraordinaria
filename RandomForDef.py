import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, auc
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.tree import plot_tree
from sklearn.model_selection import learning_curve
from sklearn.model_selection import validation_curve

# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')
# Crear una nueva variable "quality_label" basada en la variable "quality"
df['quality_label'] = np.where(df['quality'] <= 6.5, 0, 1)

# Seleccionar las columnas que vamos a utilizar en el modelo
X = df[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = df['quality_label']

# Normalizar las características
scaler = StandardScaler()
X = scaler.fit_transform(X)

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Crear el modelo y entrenarlo
model = RandomForestClassifier(n_estimators=100, random_state=42)
model.fit(X_train, y_train)

# Hacer las predicciones
y_pred = model.predict(X_test)

# Calcular la exactitud y la matriz de confusión
accuracy = accuracy_score(y_test, y_pred)
cm = confusion_matrix(y_test, y_pred)

# Calcular la sensibilidad y especificidad
tn, fp, fn, tp = cm.ravel()
sensitivity = tp / (tp + fn)
specificity = tn / (tn + fp)

# Imprimir los resultados
print("Exactitud:", accuracy)
print("Matriz de confusión:\n", cm)
print("Sensibilidad:", sensitivity)
print("Especificidad:", specificity)

# Obtener la importancia de las características
importances = model.feature_importances_
std = np.std([tree.feature_importances_ for tree in model.estimators_], axis=0)
indices = np.argsort(importances)[::-1]

# Imprimir la importancia de las características ordenadas por nombre de columna
print("Importancia de las características:")
for f in range(X.shape[1]):
    print("%d. %s (%f)" % (f + 1, df.columns[indices[f]], importances[indices[f]]))

# Graficar la matriz de confusión
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues')
plt.xlabel('Predicted')
plt.ylabel('Actual')
plt.title('Matriz de Confusión')
plt.show()

# Graficar la importancia de las características
plt.figure(figsize=(10, 6))
plt.bar(range(X.shape[1]), importances[indices], color='lightblue', yerr=std[indices], align='center')
plt.xticks(range(X.shape[1]), df.columns[indices], rotation=90)
plt.xlabel('Características')
plt.ylabel('Importancia')
plt.title('Importancia de las Características - Modelo Random Forest')
plt.tight_layout()
plt.show()

# Convertir la matriz 'X' en un objeto DataFrame
X_df = pd.DataFrame(X, columns=['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
                                'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol'])

# Seleccionar un árbol del modelo Random Forest
tree_index = 0  # Índice del árbol que deseas graficar
estimator = model.estimators_[tree_index]

# Graficar el árbol
plt.figure(figsize=(10, 10))
plot_tree(estimator, feature_names=X_df.columns, filled=True, rounded=True)
plt.show()

# curva de aprendizaje
def plot_learning_curve(model, X, y):
    train_sizes, train_scores, test_scores = learning_curve(model, X, y, cv=5, scoring='accuracy', n_jobs=-1)

    # Calcular las medias y desviaciones estándar de las puntuaciones
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    # Graficar las curvas de aprendizaje
    plt.figure()
    plt.plot(train_sizes, train_scores_mean, 'o-', color='r', label='Entrenamiento')
    plt.plot(train_sizes, test_scores_mean, 'o-', color='g', label='Validación cruzada')
    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.2,
                     color='r')
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.2,
                     color='g')
    plt.xlabel('Tamaño del conjunto de entrenamiento')
    plt.ylabel('Puntuación de precisión')
    plt.title('Curvas de Aprendizaje')
    plt.legend()
    plt.grid(True)
    plt.show()

plot_learning_curve(model, X_train, y_train)

# Curvas de validación

# Obtener los valores de los hiperparámetros que se desea evaluar
param_range = [10, 50, 100, 200, 300, 400, 500]

# Calcular las puntuaciones de validación cruzada para los diferentes valores de los hiperparámetros
train_scores, test_scores = validation_curve(RandomForestClassifier(random_state=42), X_train, y_train,
                                             param_name='n_estimators', param_range=param_range,
                                             cv=5, scoring='accuracy', n_jobs=-1)

# Calcular las medias y desviaciones estándar de las puntuaciones
train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)

# Graficar las curvas de validación
plt.figure()
plt.plot(param_range, train_scores_mean, 'o-', color='r', label='Entrenamiento')
plt.plot(param_range, test_scores_mean, 'o-', color='g', label='Validación cruzada')
plt.fill_between(param_range, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.2,
                 color='r')
plt.fill_between(param_range, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.2,
                 color='g')
plt.xlabel('Número de estimadores')
plt.ylabel('Puntuación de precisión')
plt.title('Curvas de Validación - Random Forest')
plt.legend()
plt.grid(True)
plt.show()

# Hacer las predicciones de probabilidad
y_pred_proba = model.predict_proba(X_test)[:, 1]

# Calcular la curva ROC y el AUC
fpr, tpr, thresholds = roc_curve(y_test, y_pred_proba)
roc_auc = auc(fpr, tpr)

# Graficar la curva ROC
plt.figure()
plt.plot(fpr, tpr, color='b', label='Curva ROC (AUC = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], color='r', linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('Tasa de Falsos Positivos')
plt.ylabel('Tasa de Verdaderos Positivos')
plt.title('Curva ROC')
plt.legend(loc="lower right")
plt.show()

# Imprimir el valor del AUC
print("AUC:", roc_auc)