import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import learning_curve, validation_curve
from sklearn.metrics import roc_curve, auc


# Cargar los datos
df = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Crear una nueva variable "quality_label" basada en la variable "quality"
df['quality_label'] = df['quality'].apply(lambda value: 1 if value >= 7 else 0)

# Seleccionar las columnas que vamos a utilizar en el modelo
X = df[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = df['quality_label']

# Normalizar las características
scaler = StandardScaler()
X = scaler.fit_transform(X)

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Crear el modelo de Regresión Logística y entrenarlo
model = LogisticRegression(random_state=42)
model.fit(X_train, y_train)

# Hacer las predicciones
y_pred = model.predict(X_test)

# Calcular la exactitud y la matriz de confusión
accuracy = accuracy_score(y_test, y_pred)
cm = confusion_matrix(y_test, y_pred)

# Imprimir los resultados
print("Exactitud:", accuracy)
print("Matriz de confusión:\n", cm)

# Graficar la matriz de confusión
sns.heatmap(cm, annot=True, cmap='Blues', fmt='d')
plt.xlabel('Predicción')
plt.ylabel('Valor Real')
plt.title('Matriz de Confusión - Regresión Logística')
plt.show()

# Curva de aprendizaje

# curva de aprendizaje
def plot_learning_curve(model, X, y):
    train_sizes, train_scores, test_scores = learning_curve(model, X, y, cv=5, scoring='accuracy', n_jobs=-1)

    # Calcular las medias y desviaciones estándar de las puntuaciones
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    # Graficar las curvas de aprendizaje
    plt.figure()
    plt.plot(train_sizes, train_scores_mean, 'o-', color='r', label='Entrenamiento')
    plt.plot(train_sizes, test_scores_mean, 'o-', color='g', label='Validación cruzada')
    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.2,
                     color='r')
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.2,
                     color='g')
    plt.xlabel('Tamaño del conjunto de entrenamiento')
    plt.ylabel('Puntuación de precisión')
    plt.title('Curvas de Aprendizaje - Regresión Logística')
    plt.legend()
    plt.grid(True)
    plt.show()

plot_learning_curve(model, X_train, y_train)

# Curva de validación

def plot_validation_curve(model, X, y, param_name, param_range):
    train_scores, test_scores = validation_curve(model, X, y, param_name=param_name, param_range=param_range,
                                                 cv=5, scoring='accuracy', n_jobs=-1)

    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    plt.figure()
    plt.plot(param_range, train_scores_mean, 'o-', color='r', label='Entrenamiento')
    plt.plot(param_range, test_scores_mean, 'o-', color='g', label='Validación cruzada')
    plt.fill_between(param_range, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.2,
                     color='r')
    plt.fill_between(param_range, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.2,
                     color='g')
    plt.xlabel(param_name)
    plt.ylabel('Puntuación de precisión')
    plt.title('Curva de Validación - Regresión Logística')
    plt.legend()
    plt.grid(True)
    plt.show()


param_name = 'C'
param_range = [0.001, 0.01, 0.1, 1, 10, 100]

plot_validation_curve(model, X_train, y_train, param_name, param_range)

# Obtener las probabilidades de predicción
y_prob = model.predict_proba(X_test)[:, 1]

# Calcular la tasa de falsos positivos, la tasa de verdaderos positivos y los umbrales
fpr, tpr, thresholds = roc_curve(y_test, y_prob)

# Calcular el área bajo la curva ROC
roc_auc = auc(fpr, tpr)

# Graficar la curva ROC
plt.figure()
plt.plot(fpr, tpr, color='b', label='Curva ROC (AUC = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('Tasa de Falsos Positivos')
plt.ylabel('Tasa de Verdaderos Positivos')
plt.title('Curva ROC - Regresión Logística')
plt.legend(loc="lower right")
plt.show()