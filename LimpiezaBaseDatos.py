import pandas as pd

datos = pd.read_csv('C:\\Users\\Casa\\TFG\\databes\\VinosQuality.csv')

# eliminamos duplicados y guardamos en una nueva variable
datos_sin_duplicados = datos.drop_duplicates()

# eliminamos registros vacíos y guardamos en otra nueva variable
datos_limpio = datos_sin_duplicados.dropna()

# eliminamos observaciones con valores mayores a 1 en la variable 'volatile acidity'
datos_limpio = datos_limpio[datos_limpio['volatile acidity'] <= 2]

# Eliminar filas que tengan un valor mayor que 20 en la columna "alcohol"
datos_limpio = datos_limpio[datos_limpio['alcohol'] <= 22]

# eliminamos observaciones con valores mayores a 2 en la variable 'density'
datos_limpio = datos_limpio[datos_limpio['density'] <= 2]

# guardamos el DataFrame limpio en un nuevo archivo CSV
datos_limpio.to_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv', index=False)